# GIT

## Huho

*ctrl f huho = TODO*

Prenons par pur hasard Alice et Bob, parce que ca fait A et B. Et on ajoute Charlie si besoin :^)

Alice veut developper une application.

## Git c'est quoi

Versionning, garde historique changements & versions

J'ai un fichier, je sauvergade mes changements quand c'est ce que je veux à un INSTANT T / quand ca FAIT ce que je veux / quand ca FONCTIONNE /.

## Exemple Noob (mais faut passer par la) : Faire son CV

### Les bases

Alice crée un dossier "mes_documents_pro".

```sh
# Dans "mes_documents_pro"
git init 
# Git prend connaissance des fichiers presents a la racine du projet
# Alice a créé un dossier "mes_documents_pro" dans lequel elle a créé son fichier CV.txt
# La racine du projet c'est le dossier "mes_documents_pro"
# git prend connaissance du fichier CV.
# Git fonctionne avec un sytsème de branche (expliqué plus tard). 
# Un branche nommée master est crée. 
# En anglais, les termes master & slave (Git, Ansiblen Réplication de base de données) ne sont plus....(je dis quoi la?)
# L'habitude d'avoir une branche initiale nommée main se répend (pareil, je dis quoi la ? nique.). 
```

Alice crée un fichier "CV.txt" vide dans "mes_documents_pro".

```sh
# Dans "mes_documents_pro"
git add CV.txt 
# Git comprends que le fichier CV.txt est nouveau comparé à l'etat précedent.
# La création du fichier CV.txt est enregistrée.
```

```sh
# Dans "mes_documents_pro"
git commit -m "initial commit"
# Git enregistre ce changement d'état avec le message précédent de l'option -m
# Le message initial commit, c'est un ehabitude, j'ai toujours fait ça, bonne pratique ?
```

Alice renseigne ensuite son profil & ses coordonnées dans son fichier CV.txt.

```sh
# Dans "mes_documents_pro"
git add CV.txt 
# Git comprends que le fichier CV.txt doit etre enregistré comme étant modifié.
```

```sh
# Dans "mes_documents_pro"
git commit -m "Ajout des coordonnées & du profil"
# Git enregistre ce changement avec le message précédent de l'option -m
```

Alice ajoute ensuite ses expériences professionnelles puis git add, git commit.

On revient a nos branches :

branche master : o----------------------------------o----------------------------------o
                 |                                  |                                  | commit "expériences professionnelles"
                 |commit "initial commit"           |
                                                    | commit "Ajout des coordonnées & du profil"

Voila, vous avez compris GIT (non).

## Exemple plus complet avec du Code

### Initalisation du projet

Alice souhaite créer une application qui permet à l'utilisateur de deviner un nombre en N coups. 

Exemple en python évidemment :^) *(cf cours python)*

```sh
mkdir guess_the_number
cd guess_the_number
git init
```

Tj utile d'avoir un README.md huho *(cf cours README.md / Markdown / Mermaid ?)*

```sh
# dans guess_the_number
touch README.md
git add README.md
git commit -m 'initial commit'
```

Dans le README.md *(cf cours Markdown)*

```md 
# Guess the number

Voici mon application

## How to Run

lorem ipsum

## Dropbox

lorem ipsum
```

```sh
# dans guess_the_number
git add README.md
git commit -m 'doc' # mauvaise habitude, quand c'est uniquement le readme je mets 'doc'
```

```sh
# dans guess_the_number
touch main.py
```

```python
# main.py
def guess_the_number():

    # Nb tries
    nb_tries = 10

    # The number user needs to guess
    random_number = 17

    # For loop for each try
    for nb_try in range(nb_tries):
        user_input = input('number : ')
        if user_input == random_number:
            print('Congratulations! You guessed the number.')
            break
        else:
            print('Try again')
    else:
        print(f'Sorry, you have run out of tries. The correct number was {random_number}.')


if __name__ == '__main__':
    guess_the_number()
```

```sh
# dans guess_the_number
git add main.py
git commit -m 'Création de la fonctionnalité guess_the_number'
```

On en est là : 

branche master : o----------------------------------o----------------------------------o
                 |                                  |                                  | commit "Création de la fonctionnalité guess_the_number"
                 |commit "initial commit"           |
                                                    | commit "doc"

### Partage du projet

```shell
git add remote origin git@gitlab.com:$USER/guess_the_number.git
git push
```

### Récupération du projet

```shell
git pull git@gitlab.com:$USER/guess_the_number.git
cd guess_the_number
python main.py
```

### Modifications


```mermaid
gitGraph
    commit id: "initial commit"
    checkout main
    commit id: "checkout"
    branch develop
    commit id: "update 1"
    checkout develop
    commit id: "update 2"
    checkout main
    merge develop
````
```
