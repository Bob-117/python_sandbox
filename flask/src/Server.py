# -*- coding: utf-8 -*-

__version__ = '1.0'

# Import des bibliotheques standard
from flask import Flask, request
from flask_cors import CORS
import json

# Import des ressources custom
import Conf
from Database import Database
from Item import Item
from request_test import HttpRequest


# Class
class Server:
    """
    Objet Server, librairie Flask
    Implementation de route d'API REST

    :ivar _conf: configuration reseau (ip, port) sur lequel le serveur ecoute
    """

    ###################### CONSTRUCTOR ######################
    def __init__(self):
        """
        Instancie un nouvel objet serveur
        Possede un attribut database qui est une instance de la classe Database
        """
        self.__host = Conf.SERVER_HOST
        self.__port = Conf.SERVER_PORT
        self.__db = Database()

    def start(self):
        """
        Method generale de fonctionnement et de demarrage du serveur
        On y definit ici les routes et les reponses associees
        """
        print("***start flask***")
        app = Flask(__name__)

        # some conf
        CORS(app)
        app.config['DEBUG'] = True
        app.config['CORS_HEADER'] = 'Content-Type'

        # server start => create one table if not exists
        self.get_db().create_table()
        self.http_request()

        ############# ROUTES #############
        # TODO : return response.status
        @app.route('/')
        def home():
            return "root"

        # end home

        @app.route("/item", methods=['GET'])
        def get_all():
            result = self.__db.get_all()
            return result

        # end get_all

        @app.route("/item", methods=['POST'])
        def post_one():
            data = request.json
            item_json = json.loads(data)
            item = Item(item_json['label'], item_json['price'])
            self.save(item)
            return item.get_label()

        # end post_one

        @app.route("/item", methods=['PUT'])
        def update_one():
            self.get_db().update_item(request.json, 1555555555)
            return "update_one"

        # end update_one

        @app.route("/item", methods=['DELETE'])
        def delete_one():
            self.get_db().delete_item(request.json)
            return "delete_one"

        # end delete_one

        ############# MAIN COMMAND #############
        app.run(host=self.__host, port=self.__port)

    def get_db(self):
        return self.__db

    def save(self, new_item: Item):
        # TODO : verifier la sauvegarde et renvoyer le nouvel objet si creation
        """
        Enregistrer un nouvel objet en base de donnees (suite a une requete post ligne 62)
        :param Item new_item:
        :return:
        """
        self.get_db().add_item(new_item)

    # end save
    def http_request(self):
        print("**********DEBUG***************")
        req = HttpRequest()
        # req.select_query()
        print("******************************")

