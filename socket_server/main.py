from socket_server.server import SocketServer


def process():
    s = SocketServer()
    s.start()


if __name__ == '__main__':
    process()
