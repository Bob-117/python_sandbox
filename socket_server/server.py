import datetime
import logging
import sys

import logger
import socket
import threading


class SocketServer:

    def __init__(self):
        logger.init('SocketServer')
        self.is_running = False
        self.soc = None
        self.listening_thread = None
        self.stop_event = threading.Event()
        self.ERRORS = (socket.error, OverflowError, SocketServerError)  # TODO

    def start(self):
        """
        Start the server
        Create the main socket
        Start the listening thread
        """
        self.create_socket()
        if self.is_running:
            logging.info('SocketServer running')
            self.stop_event.clear()
            self.listening_thread = threading.Thread(target=self.listening, name='SocketServer')
            # self.watcher_comm_thread.daemon = True
            self.listening_thread.start()

    def create_socket(self):
        """
        Create a main socket and bind it
        """
        tmp_soc = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
        try:
            tmp_soc.bind(('', 1117))
            tmp_soc.listen()
        except self.ERRORS as err:
            logging.error(f'Socket binding failed. : {err}')
            sys.exit('Socket binding failed')
        else:
            self.is_running = True
            self.soc = tmp_soc
        finally:
            logging.info(f'Socket set to : {self.soc}')

    def listening(self):
        """
        Listening input on main socket
        """
        while self.is_running:
            if self.stop_event.is_set():
                # its like current_thread.join() within current_thread
                break
            client_socket, add = self.soc.accept()
            try:
                client_socket.settimeout(2)
                data = client_socket.recv(16)
            except ConnectionResetError:
                logging.warning(f'Connection reset by client: {add[0]}:{add[1]}')
            except socket.timeout:
                logging.warning(f'Timeout: {add[0]}:{add[1]}')
                client_socket.sendall("?????".encode())
            else:
                data = data.decode().strip()
                logging.info(f'Received < {data} > from {add[0]}:{add[1]}')
                self.send_message(data=data, client_socket=client_socket)
            finally:
                logging.debug('end while')

    def stop(self):
        """
        Close the main socket
        Exit the listening thread
        """
        self.soc.close()
        self.is_running = False
        self.stop_event.set()
        logging.info('Server stopped')

    def send_message(self, data, client_socket):
        """
        Send a message with a socket and close it
        """
        if data == 'stop':
            response = "Stopping server..."
            client_socket.sendall(response.encode())
            client_socket.close()
            self.stop()
        else:
            responses = {
                'info': 'Server info...',
                'hour': datetime.datetime.now().strftime("%A %d %B %Y %Hh%Mm%Ss"),
                'gibson': 'SG \U0001F918'
            }
            res = responses.get(data.lower(), 'Invalid command')
            client_socket.sendall(res.encode())
            client_socket.close()


class SocketServerError(Exception):
    ...
