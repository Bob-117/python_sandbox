import socket  # Permet la connection
import threading  # Multi_thread c'est la vie
import time  # Juste pour connaitre le temps mis pour une tache
from queue import Queue  # Permet de créer des requettes

# On n'a pas envie de rester planté à attendre trop longtemps.
# Si ça ne répond pas, on coupe la connection après 1 sec
socket.setdefaulttimeout(2)
MAX_WORKERS = 12
PORTS_RANGE = range(1, 60143)
print_lock = threading.Lock()  # A comprendre


# target = input('Enter the host to be scanned: ')
SEND_MSG = False
MSG = b""
# target = "10.35.94.12"
target = "192.168.1.21"
# target = "127.0.0.1"
t_IP = socket.gethostbyname(target)  # Retourne l'adresse IP associée si on donne un nom de domaine
print('Starting scan on host: ', t_IP)


def send_message_ports(s, buffer_size=2048):
    s.send(MSG)
    data = s.recv(buffer_size)
    print("received data:", data, "Communication port opened")
    print("traduction: ", data.decode("utf-8"))


def portscan(port: int):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # à Analyser
    try:
        con = s.connect((t_IP, port))  # Se connecte en TCP à l'adresse (IP:port) voulu, TCP pour l'ACK de la connexion
        with print_lock:
            print(f"Thread{threading.get_ident()}, found open port : {port}")
            send_message_ports(s, port)
        if con is not None:
            con.close()
    except:  # Volontairement esquiver TOUTES les erreurs
        pass


def threader():
    while True:
        worker = q.get()  # Récupère la prochaine tâche à effectuer
        portscan(worker)  # Scan le port de la tâche
        q.task_done()  # Peu-importe le résultat (port OPEN ou non), on acquitte en débloquant le thread actuel


if __name__ == '__main__':
    q = Queue()  # Crée une liste de requête(s) de type<Queue>
    startTime = time.time()

    for x in range(MAX_WORKERS):  # Combien de Threads veut-on ?
        # attention, un trop grand nombre peut FORK-BOMB (ou prendre plus de temps que nécessaire)
        """
            target : la fonction à lancer.
            daemon (True/false) : permet de bloquer les Threads jusqu'à auto-destruction
        """
        thread = threading.Thread(target=threader, daemon=True)  # Prépare le processus
        thread.start()  # Lance le processus qui se mettent en attente (attention, la Queue est vide à ce moment)

    for port in PORTS_RANGE:
        q.put(port)  # Rempli la liste des taches à effectuer

    q.join()  # Lance tous les processus d'un coup et attends TOUS les résultats pour passer à la suite
    print('Time taken:', time.time() - startTime)  # Affiche la différence entre le début et la fin des opérations
