#!/bin/bash


# Params
requirements=('python' 'docker' 'docker-compose')
INSTALLED="yes"
NOT_INSTALLED="no"
declare -A requirements_dict

# Functions requirements
function check_one_command() {
  command -v $1 &>/dev/null && requirements_dict[$1]=$INSTALLED || requirements_dict[$1]=$NOT_INSTALLED
}

function check_all_requirements() {
  echo "====checking requirements===="
  for i in "${requirements[@]}"; do check_one_command ${i}; done
}

# check if python is installed on the host + what alias
function check_python () {
  python_commands=('python' 'python3' 'py')
  for i in "${python_commands[@]}"
  do
      OUTPUT=$(command -v ${i})
      if [ -n "${OUTPUT}" ];
      then
        echo ${i} "found"
        MAIN_PYTHON="${i}"
        return 1
      fi
  done
}

check_all_requirements

for key in "${!requirements_dict[@]}"; do
  echo "$key : ${requirements_dict[$key]}"
done

echo "====checking python alias===="
if [ "${requirements_dict[python]}" = "${NOT_INSTALLED}" ];
then
  check_python
else
  MAIN_PYTHON=python
fi

#echo "${MAIN_PYTHON}"

# Function deploy
fucntion run_iam() {
  cd ../IAM/
}

function run_api() {

}

# create venv
function setup_env () {
  command cd ../api
  ${MAIN_PYTHON} -m virtualenv venv
  ${MAIN_PYTHON} -m pip list
  ${MAIN_PYTHON} -m pip install -r requirements.txt
}


# get an avalaible python command
check_python
MAIN_PYTHON=$MAIN_PYTHON_FOUND

# exit if no avalaible python, else create env and run project
[[ -n "${MAIN_PYTHON}" ]] && setup_env ${MAIN_PYTHON} || { echo "Quitting, not python found, please install it"; exit 1; }

