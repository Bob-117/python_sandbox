# -*- coding: utf-8 -*-

def fact(n):
    return n if n == 1 else n * fact(n - 1)


num = 117

print("negativ" if num < 0 else (1 if num == 0 else fact(num)))
