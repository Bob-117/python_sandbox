import numpy as np


def handle_new_predict(predict):
    print(f'Data identified as target{predict} !')


def one(x):
    handle_new_predict(x)


def two(x):
    handle_new_predict(x)


def three(x):
    handle_new_predict(x)


def default_handling(x):
    print(f'{x} is not a labeled target, but is {"odd" if x % 2 == 0 else "even"}')
    # print(f'- {x:{" is odd" if x % 2 ==0 else "is even"}}')


def if_else_case(predict_target):
    if predict_target == 1:
        one(predict_target)
    elif predict_target == 2:
        two(predict_target)
    elif predict_target == 3:
        three(predict_target)
    else:
        default_handling(predict_target)


def dict_case(predict_target):
    actions = {
        1: one,
        2: two,
        3: three
    }
    action = actions.get(predict_target, default_handling)
    action(predict_target)


if __name__ == '__main__':
    for _ in range(10):
        predict_target = np.random.randint(9, size=1)[0]  # oof
        print(f'---------------')
        print(f'--', end='')
        if_else_case(predict_target)
        print(f'--', end='')
        dict_case(predict_target)
