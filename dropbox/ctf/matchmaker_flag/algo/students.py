import random


class Student:

    def __init__(self, _name, _notes):
        self.name = _name
        self.notes = _notes

    def max_note(self):
        return max(self.notes.values())

    def preferred_student(self):
        _preferred_student = [_student for _student, _associated_note in self.notes.items() if
                              _associated_note == self.max_note()]
        if len(_preferred_student) != 1:
            print('idk but maybe we have to handle many preferred students ?')
            output = _preferred_student[0]
        else:
            output = _preferred_student[0]
        return output

    def match(self, other_student):
        # print('--------------')
        # print(self, self.max_note(), self.preferred_student())
        # print(other_student, other_student.max_note(), other_student.preferred_student())
        return self.preferred_student() == other_student.name and other_student.preferred_student() == self.name

    def __str__(self):
        # return f'-Student{self.name}- {self.notes}|{self.max_note()} - {self.preferred_student()}'
        return f'-Student{self.name}-{self.notes}'

    def __repr__(self):
        return self.__str__()


def generate_data():
    n = random.randint(25, 50) * 2
    for _ in range(n - 1):
        print(_)


def _print(_notes):
    for student, note_raw in enumerate(_notes):
        print(
            f'Student n{student + 1}: {note_raw}, max note = {max(note_raw.values())} for student {[k for k, v in note_raw.items() if v == max(note_raw.values())]}')


def filter_match(student, match_list):
    out = [relation for relation in match_list if student in relation]
    if len(out) != 0:
        print(f'{student} already in match')
    return out


def create_all_student_list(_input):
    return [Student(_name=student + 1, _notes=note_raw) for student, note_raw in enumerate(_input)]


def available_students_list(all_students, current_student, match_list):
    return (
        filter(
            lambda _other:
            _other.name != current_student.name
            and len(filter_match(current_student, match_list)) == 0
            and len(filter_match(_other, match_list)) == 0,
            all_students)
    )


def main_process():
    notes = [
        {2: 1000, 3: 8, 4: 12},
        {1: 1000, 3: 8, 4: 5},
        {1: 10, 2: 8, 4: 12},
        {1: 10, 2: 8, 3: 0},
    ]

    match = []
    all_students = create_all_student_list(notes)
    all_students_copy = all_students.copy()
    # print('Preferred student for each student :')
    # print(list(map(lambda _student: f'{_student.name} : {_student.preferred_student()}', all_students)))
    stop = 0
    while len(match) < (len(all_students) / 2) and stop < 10:
        print(f'>>>>>>>>LOOP {stop}')
        for current_student in all_students_copy:
            for other_available_student in available_students_list(all_students, current_student, match_list=match):
                if current_student.match(other_available_student):
                    print(f'MATCH Student n{current_student.name} - Student n{other_available_student.name}')
                    match.append((current_student, other_available_student))
                    all_students_copy.remove(current_student)
                    all_students_copy.remove(other_available_student)
                else:
                    current_student.notes[other_available_student.name] = -1
            print(all_students_copy)
            stop += 1
        print(match)
        # for current_student in all_students:
        #     print(f'===== {current_student} ======')
        #     available_students = list(
        #         filter(
        #             lambda _other:
        #             _other.name != current_student.name
        #             and len(filter_match(current_student, match)) == 0
        #             and len(filter_match(_other, match)) == 0,
        #             all_students)
        #     )
        #     for other in available_students:
        #         if current_student.match(other):
        #             print(f'MATCH {current_student} - {other}')
        #             match.append((current_student, other))
        #         else:
        #             current_student.notes[other.name] = -1

        # for other in all_students:
        #     if other != student and len(filter_match(student, match)) == 0:
        #         if student.match(other):
        #             print(f'MATCH {student} - {other}')
        #             match.append((student, other))
        #         else:
        #             student.notes[other.name] = -1


if __name__ == '__main__':
    # _print()
    # main_process()
    generate_data()
