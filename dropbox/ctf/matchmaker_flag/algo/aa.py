import socket
import time
import numpy as np
from pwn import *

from scipy.optimize import linear_sum_assignment


class NetCatSrv:
    def __init__(self, host, port) -> None:
        self.remote = remote(host, port)

    def get_message(self) -> str:
        return self.remote.recvuntil(b">", drop=True).decode(encoding="UTF-8")

    def send_message(self, message: str):
        self.remote.send(message.encode())
        # self.socket.send(message.encode())
        # self.socket.shutdown(socket.SHUT_WR)


def parse_input_numpy(raw_list: str) -> np.ndarray:
    print(type(raw_list))
    list_student = raw_list.split("\n")
    list_student.pop(len(list_student) - 1)
    cast_int = list(map(
        lambda sub: [int(value) for value in sub.split(' ')],
        list_student
    ))
    all_notes = np.zeros(shape=(len(list_student), len(list_student) - 1))
    for id_student in range(len(cast_int)):
        for id_note in range(len(cast_int) - 1):
            all_notes[id_student][id_note] = cast_int[id_student][id_note]
    # print(all_notes.shape)
    return all_notes


def reshape_notes(_mat):
    size = _mat.shape[0]
    _new_mat = np.zeros((size, size))
    for i in range(size):
        for stu, note in enumerate(_mat[i]):
            if i <= stu:
                stu += 1
            _new_mat[i][stu] += note
    return _new_mat


def scores(_mat):
    size = _mat.shape[0]
    _scores = np.zeros((size, size))
    for i in range(size):
        for j in range(i + 1):
            _scores[i][j] = _scores[j][i] = _mat[i][j] + _mat[j][i]
    return _scores


def scipy_process(_mat):
    row_ind, col_ind = linear_sum_assignment(-_mat)
    pairs = list(zip(row_ind, col_ind))
    _final_pairs = []
    for i, (x, y) in enumerate(pairs):
        if x not in [pair[0] for pair in _final_pairs] and y not in [pair[1] for pair in _final_pairs]:
            _final_pairs.append((x, y))
            _final_pairs.append((y, x))
    return _final_pairs


def parse_result(list_pair) -> str:
    resp = ""
    for value in list_pair:
        resp += f"{value[0]},{value[1]};"
    resp = resp[:-1]
    return resp


def main_process():
    nc = NetCatSrv("0.cloud.chals.io", 22304)
    # with open("test_list.txt", "r") as list_file:
    #     raw_list = list_file.read()
    raw_list = nc.get_message()
    print(raw_list)
    print(type(raw_list))
    all_notes = parse_input_numpy(raw_list)
    reshape_mat = reshape_notes(all_notes)
    scores_mat = scores(reshape_mat)
    resp = scipy_process(scores_mat)
    str_resp = parse_result(resp)
    nc.send_message(str_resp)
    # print(raw_list, end="")
    print(str_resp)
    # raw_list2 = nc.remote.recvline()
    # print("raw_list2 : " + raw_list2)
    # nc.send_message(str_resp)


if __name__ == '__main__':
    main_process()
