import random
import numpy as np


def generate_data():
    # n = random.randint(25, 50) * 2
    n = 4
    print(f'Nb of students : {n}')
    all_notes = np.zeros((n, n - 1))
    for id_student in range(n):
        for nb_notes in range(n - 1):
            all_notes[id_student][nb_notes] = random.randint(0, 100)
    return all_notes

def mark_matrix(mat):
    '''
    Finding the returning possible solutions for LAP problem.
    '''

    # Transform the matrix to boolean matrix(0 = True, others = False)
    cur_mat = mat
    zero_bool_mat = (cur_mat == 0)
    zero_bool_mat_copy = zero_bool_mat.copy()

    # Recording possible answer positions by marked_zero
    marked_zero = []
    while (True in zero_bool_mat_copy):
        min_zero_row(zero_bool_mat_copy, marked_zero)

    # Recording the row and column positions seperately.
    marked_zero_row = []
    marked_zero_col = []
    for i in range(len(marked_zero)):
        marked_zero_row.append(marked_zero[i][0])
        marked_zero_col.append(marked_zero[i][1])

    # Step 2-2-1
    non_marked_row = list(set(range(cur_mat.shape[0])) - set(marked_zero_row))

    marked_cols = []
    check_switch = True
    while check_switch:
        check_switch = False
        for i in range(len(non_marked_row)):
            row_array = zero_bool_mat[non_marked_row[i], :]
            for j in range(row_array.shape[0]):
                # Step 2-2-2
                if row_array[j] == True and j not in marked_cols:
                    # Step 2-2-3
                    marked_cols.append(j)
                    check_switch = True

        for row_num, col_num in marked_zero:
            # Step 2-2-4
            if row_num not in non_marked_row and col_num in marked_cols:
                # Step 2-2-5
                non_marked_row.append(row_num)
                check_switch = True
    # Step 2-2-6
    marked_rows = list(set(range(mat.shape[0])) - set(non_marked_row))

    return (marked_zero, marked_rows, marked_cols)


def hungarian_algorithm(mat):
    dim = mat.shape[0]
    cur_mat = mat

    # Step 1 - Every column and every row subtract its internal minimum
    for row_num in range(mat.shape[0]):
        cur_mat[row_num] = cur_mat[row_num] - np.min(cur_mat[row_num])

    for col_num in range(mat.shape[1]):
        cur_mat[:, col_num] = cur_mat[:, col_num] - np.min(cur_mat[:, col_num])
    zero_count = 0
    while zero_count < dim:
        # Step 2 & 3
        ans_pos, marked_rows, marked_cols = mark_matrix(cur_mat)
        zero_count = len(marked_rows) + len(marked_cols)

        if zero_count < dim:
            cur_mat = adjust_matrix(cur_mat, marked_rows, marked_cols)

    return ans_pos


def main_process():
    _notes = generate_data()
    print(_notes)
    ans_pos = hungarian_algorithm(_notes.copy())
    print(ans_pos)


class Student:

    def __init__(self, _name, _notes):
        self.name = _name
        self.notes = _notes

    def max_note(self):
        return max(self.notes.values())

    def preferred_student(self):
        _preferred_student = [_student for _student, _associated_note in self.notes.items() if
                              _associated_note == self.max_note()]
        if len(_preferred_student) != 1:
            print('idk but maybe we have to handle many preferred students ? (for now we take the first in the list)')
            output = _preferred_student[0]
        else:
            output = _preferred_student[0]
        return output

    def match(self, other_student):
        # print('--------------')
        # print(self, self.max_note(), self.preferred_student())
        # print(other_student, other_student.max_note(), other_student.preferred_student())
        return self.preferred_student() == other_student.name and other_student.preferred_student() == self.name

    def __str__(self):
        # return f'-Student{self.name}- {self.notes}|{self.max_note()} - {self.preferred_student()}'
        return f'-Student{self.name}-{self.notes}'

    def __repr__(self):
        return self.__str__()


if __name__ == '__main__':
    main_process()
