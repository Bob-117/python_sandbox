class DB:

    items = [
        {'nom': 'Young', 'prenom': 'Angus'},
        {'nom': 'OKeeffe', 'prenom': 'Joel'},
        {'nom': 'Hendrix', 'prenom': 'Jimi'},
        {'nom': 'Halford', 'prenom': 'Rob'},
    ]

    def insert_into(self, item):
        self.items.append(item)

    def select(self):
        return self.items
