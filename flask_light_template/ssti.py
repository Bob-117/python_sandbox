import requests as requests

_url = 'http://127.0.0.1:17117/ssti'

payloads = [
    " {{ 100 + 17 }} ",
    " {{ config }} ",
    "10 + 7",
    "gibson"
]
if __name__ == '__main__':
    for payload in payloads:
        post = requests.post(url=_url, data={'payload': payload})
        print(f'payload = {payload} : res = {post.content}')


